## Добавление поддержки Maven-а в проект

Для добавления поддержки Maven необходимо:
1. Нажать правой кнопкой на название проекта и выбрать из выпадающего меню пункт `"Add Framework Support"`
![1.jpg](1.jpg)
2. В появивщемся окне `"Add Framework Support"` пометить галочкой `"Maven"` и нажать кнопку `Ok`
![2.jpg](2.jpg)

В результате автоматически в проект будет добавлен файл `pom.xml` 


***Примечание.*** 

>Для проектов JavaFX:
>- пренести файлы `*.fxml` в папку `resource`
>- в методе `main` заменить
>```java
>Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
>```
>на
>```java
>Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sample.fxml"));
>```

