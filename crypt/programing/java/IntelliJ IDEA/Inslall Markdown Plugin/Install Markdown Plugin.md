### Установка плагина Markdown 

Для добавления возможности создавать и редактировать файлы в формате markdown в IntelliJ IDE необходимо:
1. Перейти к настройкам `File->Settings..`.
2. В окне настроек перейти к разделу `"Plugins"`, в окне поиска вбить `"markdown"` и нажать `"Search in repositories"`.

![1.jpg](1.jpg)

3. В окне `"Browse Repositories"` выбрать плагин `"Markdown support"` и нажать в правом окне кнопку `"Install"`.
![2.jpg](2.jpg)

4. Перезапустить Срезу разработки IntelliJ IDEA, нажав кнопку `"Restart IntelliJ IDEA`.
![3.jpg](3.jpg)

В результате, после перезапуска IDEA появится возможность редактировать файлы формата markdown (*.md) в интерактивном виде
![4.jpg](4.jpg)
