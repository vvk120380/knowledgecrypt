## Пишем элегантный и чистый Java код в 5 этапов

Чтобы Java код был более понятным и простым, используются библиотеки, методы и инструменты Java Core. Рассмотрим некоторые из них.

---

## Аккуратный Java код

Самый простой способ быстренько «набросать» программу – использовать классы JavaBeans, которые пишутся в соответствии с некоторыми правилами. Например:

```java
public class DataHolder {
    private String data;

    public DataHolder() {
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return this.data;
    }
}
```

 Слишком много лишних условий. Даже если IDE автоматически генерирует такой код (как при стандартном создании конструктора), исправляйте его, [и вот почему](http://www.javapractices.com/topic/TopicAction.do?Id=84). Вместо этого лучше применить стиль C-структур, который позволит хранить данные:

```java
public class DataHolder {
    public final String data;

    public DataHolder(String data) {
        this.data = data;
    }
}
```

Это сокращение количества строк кода наполовину. Кроме того, данный класс является неизменным, поэтому и работать с ним проще.

Если вы храните объекты Map или List, которые могут быть легко изменены, используйте ImmutableMap или ImmutableList.

---

## Шаблон Builder

Если у вас есть сложный объект, пригодится шаблон Builder.

Вы создаете статический внутренний класс, который будет строить ваш объект. Он использует изменяемое состояние, но как только вы вызываете build, он выдает неизменяемый объект.

Представьте, что у нас есть более сложный **DataHolder**. Builder может выглядеть так:

```java
public class ComplicatedDataHolder {
    public final String data;
    public final int num;

    public static class Builder {
        private String data;
        private int num;
        
        public Builder data(String data) {
            this.data = data;
            return this;
        }

        public Builder num(int num) {
            this.num = num;
            return this;
        }

        public ComplicatedDataHolder build() {
            return new ComplicatedDataHolder(data, num);
        }  
    }
}
```

Чтобы применить его:

```java
final ComplicatedDataHolder cdh = new ComplicatedDataHolder.Builder()
    .data("отправить это")
    .num(523)
    .build();
```

[Существует гораздо больше примеров](https://jlordiales.me/2012/12/13/the-builder-pattern-in-practice/). Это дает вам неизменяемые объекты и более свободный интерфейс. Вместо того, чтобы писать объекты-конструкторы, рассмотрите возможность использования одной из библиотек, помогающих задействовать билдеры.

---

## Непрерывное создание объектов

Если вы создаете много неизменяемых объектов вручную, примените обработчик аннотаций для автоматического создания их из интерфейсов. Это минимизирует код шаблона и уменьшит вероятность ошибок. Посмотрите [эту презентацию](https://docs.google.com/presentation/d/14u_h-lMn7f1rXE1nDiLX0azS3IkgjGl5uxp5jGJ75RE/edit#slide=id.g2a5e9c4a8_00) для понимания проблем с обычными шаблонами в [Java-кодинге](https://proglib.io/p/cool-java-tricks/).

[Обработчик аннотации для создания неизменяемых объектов и билдеров](https://github.com/immutables/immutables).

---

## Exception

[Проверенные исключения](https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html) следует использовать с осторожностью. Они заставляют добавлять много try/catch блоков и заворачивать в них исключения. Лучше использовать непроверенные исключения RuntimeException, которые свидетельствуют об ошибке со стороны разработчика и не требуют постановки try/catch. С RuntimeException код становится намного чище.

Пример использования:

```java
public int calculateSquare(Rectange rect) {
	if (rect == null) {
		throw new NullPointerException(“Rectangle can't be null”);
	}
```

---

## Внедрение зависимости

Это, скорее, раздел разработки ПО в целом, чем раздел Java, но одним из лучших способов написания тестируемого программного обеспечения является применение зависимостей.

В Java код это обычно внедряется с помощью [фреймворка Spring](http://projects.spring.io/spring-framework/). Если используете конфигурацию XML, важно [не злоупотреблять Spring](https://softwareengineering.stackexchange.com/questions/92393/what-does-the-spring-framework-do-should-i-use-it-why-or-why-not) из-за его формата конфигурации на базе XML. В XML-файле не должно быть логических или контрольных структур: только зависимости.

Хорошие альтернативы Spring – Google и Square [Dagger](http://square.github.io/dagger/) или Google [Guice](https://github.com/google/guice).

---

## Избегайте Null

Избегайте использования null, если это возможно. Не возвращайте нулевые коллекции, когда коллекция должна быть пустой. Если вы собираетесь использовать null, рассмотрите аннотацию [@Nullable](https://github.com/google/guice/wiki/UseNullable). Например, [IntelliJ IDEA](https://www.jetbrains.com/idea/) включает в себя встроенную поддержку для удобного ознакомления с аннотациями.

- [Избегание Null с помощью «Сообщить, не спрашивать»](http://www.natpryce.com/articles/000777.html)
- [Избегание Null с полиморфной отправкой](http://www.natpryce.com/articles/000778.html)

Если вы используете [Java 8](http://www.java8.org/), отдайте предпочтение [Optional](http://www.oracle.com/technetwork/articles/java/java8-optional-2175753.html). Используйте его следующим образом:

```java
public class FooWidget {
    private final String data;
    private final Optional<Bar> bar;

    public FooWidget(String data) {
        this(data, Optional.empty());
    }

    public FooWidget(String data, Optional<Bar> bar) {
        this.data = data;
        this.bar = bar;
    }

    public Optional<Bar> getBar() {
        return bar;
    }
}
```

```java
final Optional<FooWidget> fooWidget = maybeGetFooWidget();
final Baz baz = fooWidget.flatMap(FooWidget::getBar)
                         .flatMap(BarWidget::getBaz)
                         .orElse(defaultBaz);
```

Ясно, что данные никогда не будут нулевыми, но условие при этом может выполняться или не выполняться. Единственный недостаток Optional – это то, что стандартная библиотека находится без хорошей поддержки, поэтому в ней по-прежнему требуется использование null.